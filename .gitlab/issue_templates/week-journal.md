<!-- Instructions 
[ ] Title this issue as WXX YYYY-MM-DD
[ ] Update month, icon, week, and dates
[ ] Add links to previous and next weekly issues
[ ] Add milestone planning issue link
[ ] Update availability
[ ] Add goals
[ ] Copy future work from the previous week and add any new topics
-->

# Month :icon: Wxx | MMM xx - MMM xx

🧾 [Issues assigned to me](https://gitlab.com/dashboard/issues?assignee_username=michelletorres)

[:arrow_backward:  Previous week](issue-link) • [Next week :arrow_forward:](issue-link)

Current milestone: 

## :palm_tree: Availability / Out of office (OoO)

**Key:** 
:thermometer: Out Sick
:sun_with_face: Family and Friends Day
:palm_tree: Vacation
:ferris_wheel: Public Holiday 

* Monday 6:00 - 15:00 CET (GMT +1)
* Tuesday 6:00 - 15:00 CET (GMT +1)
* Wednesday 6:00 - 15:00 CET (GMT +1)
* Thursday 6:00 - 15:00 CET (GMT +1)
* Friday 6:00 - 15:00 CET (GMT +1)



_Read here about [my "normal" schedule and my working style](https://gitlab.com/michelletorres/readme#my-working-style)_
  


## :dart:  Goals

**Key:** 
:handshake_tone3: People
:tools: Process 
:chart_with_upwards_trend: Product


Important: _The following is my main focus for the week. Routine work like 1:1s, team meetings, comments, reviews, etc., should continue._   

<details>
<summary>Mind process behind this prioritization</summary>



</details>
 


**What**  

* :one: xyz  
* :two: xyz
* :three: xyz

**How**  

1. xyz
    1. [ ] xyz
1. 🛠 Weekly team progress updates
   1. [ ] [Ally updates](https://app.ally.io/teams/44662/objectives?tab=0&chartView=false&time_period_id=155986) (internal)
   1. [ ] Async update https://gitlab.com/groups/gitlab-org/ci-cd/package-stage/-/epics/10+
1. 🛠 [Package reports](https://gitlab.com/gitlab-org/quality/triage-reports/-/issues/?label_name%5B%5D=devops%3A%3Apackage)
    1. [ ] Triage

<details><summary>:mag: [Keeping myself informed](https://about.gitlab.com/handbook/engineering/#keeping-yourself-informed)</summary>

1. [ ] [engineering allocation](https://docs.google.com/document/d/164hNObllaLWosG110-A0UouYlcaqOxbPpHATFD38_Gw/edit)
1. [ ] [eng-week-in-review](https://docs.google.com/document/d/1JBdCl3MAOSdlgq3kzzRmtzTsFWsTIQ9iQg0RHhMht6E/edit#)
1. [ ] [development staff](https://docs.google.com/document/d/1OaJBu_4hIZV5sZj3a-BEba82XgWeyVAt3EwVBTowq5w/edit#)
1. [ ] [engineering staff](https://docs.google.com/document/d/1uaBOKgqYM5-pDWBgfAuOqFYDAA56yVz76lgMvv8jUCQ/edit#)
1. [ ] [cto office hours](https://docs.google.com/document/d/1UKAK51eyy7dOA9pRWz_VDEVhO6c2VBHoQ6MUB0RdvM8/edit#)
1. [ ] [development EM office hours](https://docs.google.com/document/d/1SVIDKHM738cHqP6lw_37W3knUVTEAaeDkITWEGy-JBM/edit)
1. [ ] [ops staff](https://docs.google.com/document/d/1LnFPO2_nmb7s1TvqsT3DGSXa1MEXkG4KDL8IO4NqXW8/edit#)
1. [ ] [cd staff](https://docs.google.com/document/d/1eqXfSKZufbOgUHROOcgvTfXO455FJcg5hCgWsM0F7Ss/edit#)
1. [ ] [package staff](https://docs.google.com/document/d/1ZtGAm3e3v-HuRp6nzvpvgf89i6YP4OEAr4IAaul-AyM/edit#)
1. [ ] [ops hiring](https://docs.google.com/document/d/1jl407WpUkPx9wc_ZQHGn77CywMURL9NS7ONWoLpGGds/edit#)


</details>

**What I would like to do, but may not be able to do...**

1. [ ] xyz


/assign @michelletorres
/due next monday
