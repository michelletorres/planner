# 🎯 2022 Goals and Planner

**:handshake_tone3: Team synergy and team health**
- Build trusting working relationships
- Hire the right people
- Grow team members

**:chart_with_upwards_trend: Results towards team’s vision**
- Everyone understands why are we here and where are we going
- We rely on each other to achieve more



## January

<details>
<summary>
    Expand
</summary>

### [W01 (Jan 3 - Jan 7)](https://gitlab.com/michelletorres/readme/-/issues/19)
* :handshake_tone3:  Promotions projection  
* :handshake_tone3:  Talent Assessment and career development


### [W02 (Jan 10 - Jan 14)](https://gitlab.com/michelletorres/readme/-/issues/20)
- :handshake: FY23 Annual compensation review
- :chart_with_upwards_trend: FY23-Q1 OKRs

### W03 (Jan 17 - Jan 20) :palm_tree: PTO

### W04 (Jan 24 - Jan 28) :palm_tree: PTO

</details>

## February

<details>
<summary>
    Expand
</summary>

### [W05 (Jan 31 - Feb 04)](https://gitlab.com/michelletorres/readme/-/issues/21)

### [W06 (Feb 07 - Feb 11)](https://gitlab.com/michelletorres/readme/-/issues/22)
- :chart_with_upwards_trend: FY23-Q1 OKRs
- :handshake_tone3: Compensation updates
- :handshake_tone3: Hiring


### [W07 (Feb 14 - Feb 18)](https://gitlab.com/michelletorres/readme/-/issues/23)

- :handshake_tone3: Hiring
- :chart_with_upwards_trend: Plan reliability work

### [W08 (Feb 21 - Feb 25)](https://gitlab.com/michelletorres/readme/-/issues/24)

</details>

## March

<details>
<summary>
    Expand
</summary>

### [W09 (Feb 28 - Mar 04)](https://gitlab.com/michelletorres/readme/-/issues/25)

- :handshake_tone3: Hiring

### [W10 (Mar 07 - Mar 11)](https://gitlab.com/michelletorres/readme/-/issues/26)

- :handshake_tone3: Hiring

### [W11 (Mar 14 - Mar 18)](https://gitlab.com/michelletorres/readme/-/issues/27)

- :handshake_tone3: Hiring

### [W12 (Mar 21 - Mar 25)](https://gitlab.com/michelletorres/readme/-/issues/28)

- :handshake_tone3: Hiring

### [W13 (Mar 28 - Apr 1)](https://gitlab.com/michelletorres/readme/-/issues/29)

- :handshake_tone3: Hiring

</details>

## April

### [W14 (Apr 4 - Apr 8)](https://gitlab.com/michelletorres/readme/-/issues/30)

- :handshake_tone3: Hiring

### [W15 (Apr 11 - Apr 15)](https://gitlab.com/michelletorres/readme/-/issues/31) :palm_tree: PTO


### [W16 (Apr 18 - Apr 22)](https://gitlab.com/michelletorres/readme/-/issues/32)

- :chart_with_upwards_trend: FY23-Q2 OKRs
- :handshake_tone3: Hiring

### [W17 (Apr 25 - Apr 28)](https://gitlab.com/michelletorres/readme/-/issues/33)


## May

<details>
<summary>
    Expand
</summary>

- :chart_with_upwards_trend: Plan reliability work
Crucial conversations (May 3, 10, 17 and 24)

</details>






