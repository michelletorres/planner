# Michelle's Planner

This is planner so I can focus on what matters of my day-to-day work at GitLab.

## How to use it
- A high level planner for the year in a [single file](https://gitlab.com/michelletorres/planner/-/blob/main/2022-planner.md).
- A weekly tracker in [issues](https://gitlab.com/michelletorres/planner/-/blob/main/2022-planner.md) listing the goals, the tasks to achieve those goals and the learnings at the end of the day.

## About me
- [ReadMe](https://gitlab.com/michelletorres/michelletorres/-/blob/main/README.md)

---

Feel free to contribute to this project and [weekly template](https://gitlab.com/michelletorres/planner/-/blob/main/.gitlab/issue_templates/week-journal.md) by opening a [merge request](https://gitlab.com/michelletorres/planner/-/merge_requests)
